const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

/*test('Player should have 100 chips', () => {
	let hashname;
	return post('/players')
	.then(response =>{ 
		hashname = response.hashname;
		return get('/chips', hashname);
       })
	.then(response => expect(response.chips).toEqual(100))
});*/

//ERROR NUMBER 1
test('Black should double the number of chips if number 11 is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/black', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 11 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(200))
});

//ERROR NUMBER 2
test('Red should double the number of chips if number 12 is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/red', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 12 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(200))
});

//ERROR NUMBER 3
test('Bet even should lose the number of chips if number ' + 0 + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/even', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(0))
	}
);

//ERROR NUMBER 4
test('Bet high numbers should double the number of chips if number 19 is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/high', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + 19 , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(200))
	}
);

//ERROR NUMBER 5
test('Bet straight should multiply x35 the number of chips if number 4 is spinned', () => //PENDIENTE DE MIRAR DEBERIA DE CASCAR EL 4 PERO ME DA UN ERROR EXTRAÑO
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/straight/' + 4, hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 4 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(3600));
});

//ERROR NUMBER 6 --> Unexpected screen output
test.each([31,32,34,35])('Bet on corner 31-32-34-35 should multiply x9 the number of chips if number  %d is spinned', (number) =>
	 post('/players')
	.then((response) => 
	{
		hashname = response.hashname;
		return post('/bets/corner/31-32-34-35', hashname, {chips:100}); // a bet
	})
	.then((response) => post('/spin/' + number, hashname)) // spin the wheel
	.then((response) => get('/chips', hashname)) // checking the number of chips
	.then((response) => expect(response.chips).toEqual(900))
);

//ERROR NUMBER 7
test.each([1,2,3,4,5,6])('Bet on line number [1-2-3-4-5-6] should multiply by 6 the number of chips if number %d is spinned', (number) =>
	post('/players')
	.then((response) => 
	{
		hashname = response.hashname;
		return post('/bets/line/1-2-3-4-5-6', hashname, {chips:100}); // a bet
	})
	.then((response) => post('/spin/' + number, hashname)) // spin the wheel
	.then((response) => get('/chips', hashname)) // checking the number of chips
	.then((response) => expect(response.chips).toEqual(600))
);	

//ERROR NUMBER 8
test.each([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34])
	('Bet on split should multiply the number of chips x17 if number %d is spinned' , (number) =>
		post('/players')
		.then((response) => 
		{
			hashname = response.hashname;
			let value = number+1;
			return post('/bets/split/'+ number + '-' + value, hashname, {chips:100}); // a bet
		})
		.then((response) => post('/spin/' + number, hashname)) // spin the wheel
		.then((response) => get('/chips', hashname)) // checking the number of chips
		.then((response) => expect(response.chips).toEqual(1800))
);

//ERROR NUMBER 9
test('Bet low numbers should lose the number of chips if number 0 is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/low', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});


/*for(let number of[1,4,7,10,13,16,19,22,25,28,31,34])
{
	test('Bet column1 should triple the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/column/1', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(300))
	});
}

for(let number of[2,5,8,11,14,17,20,23,26,29,32,35])
{
	test('Bet column2 should triple the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/column/2', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(300))
	});
}

for(let number of[3,6,9,12,15,18,21,24,27,30,33,36])
{
	test('Bet column3 should triple the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/column/3', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(300))
	});
}*/

/*for(let number of[1,2,3,4,5,6,7,8,9,10,11,12]) NO HACE NADA
{
	test('Bet dozen 1 should triple the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/dozen/1', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(300))
	});
}*/

/*for(let number of[13,14,15,16,17,18,19,20,21,22,23,24]) 
{
	test('Bet dozen 2 should triple the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/dozen/2', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(300))
	});
}

for(let number of[25,26,27,28,29,30,31,32,33,34,35,36])
{
	test('Bet dozen 3 should triple the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/dozen/3', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(300))
	});
}*/

/*for(let number of[2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36]) //NO DETECTA ERRORES
{
	test('Bet even should double the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/even', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(200))
	});
}*/

/*for(let number of[1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]) NO CASCA
{
	test('Bet odd should double the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/odd', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(200))
	});
}*/

/*for(let number of[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]) // NO CASCA
{
	test('Bet low numbers should double the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/low', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(200))
	});
}*/

/*for(let number of[19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) //Solamente casca con 19
{
	test('Bet high numbers should double the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/high', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(200))
	});
}*/

/*for(let number of[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) //Solamente casca con 4
{
	test('Bet straight should multiply x36 the number of chips if number ' + number + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/straight/' + number, hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + number , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(3600))
	});
}*/

/*test('Bet colour red should lose the number of chips if number 0 is spinned', () => //NO CASCA
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/red', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});*/

/*test('Bet colour black should lose the number of chips if number 0 is spinned', () => //NO CASCA
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/black', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});*/

/*test.each([0,2,3])('Bet on street 0-2-3 should multiply the number of chips x11 if number %d is spinned' , (number) =>
		post('/players')
		.then((response) => 
		{
			hashname = response.hashname;
			return post('/bets/street/0-2-3', hashname, {chips:100}); // a bet
		})
		.then((response) => post('/spin/' + number, hashname)) // spin the wheel
		.then((response) => get('/chips', hashname)) // checking the number of chips
		.then((response) => expect(response.chips).toEqual(1200))
);*/

/*test('Bet dozen 1 should lose the number of chips if number ' + 0 + ' is spinned', () => 
	{
		return post('/players')
		.then(response => 
		{
			hashname = response.hashname;
			return post('/bets/dozen/1', hashname, {chips:100}); // a bet
		})
		.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
		.then(response => get('/chips', hashname)) // checking the number of chips
		.then(response => expect(response.chips).toEqual(0))
	});

test('Bet dozen 2 should lose the number of chips if number ' + 0 + ' is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/dozen/2', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});

test('Bet dozen 3 should lose the number of chips if number ' + 0 + ' is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/dozen/3', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});*/

/*test('Bet column1 should lose the number of chips if number ' + 0 + ' is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/column/1', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});

test('Bet column1 should lose the number of chips if number ' + 0 + ' is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/column/2', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});

test('Bet column1 should lose the number of chips if number ' + 0 + ' is spinned', () => 
{
	return post('/players')
	.then(response => 
	{
		hashname = response.hashname;
		return post('/bets/column/3', hashname, {chips:100}); // a bet
	})
	.then(response => post('/spin/' + 0 , hashname)) // spin the wheel
	.then(response => get('/chips', hashname)) // checking the number of chips
	.then(response => expect(response.chips).toEqual(0))
});*/